import './Header.css';
import Dropdown from "react-bootstrap/Dropdown";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import Auth from "../../Auth/Auth";

const menu = {
  height: "50px",
  fontSize: "24px",
  cursor: "pointer",
  display: "flex",
  alignItems: "center",

};

export default function Header(props) {

  let { isLogout } = Auth();
  return (
    <>
      <header className="d-flex justify-content-between">
        <div className="trigger ps-3 pt-2" style={menu} onClick={props.toggler}>
          <FontAwesomeIcon icon={faBars} />
        </div>
       
          <Dropdown className="pt-2 pe-3">
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item href="/">Login</Dropdown.Item>
              <Dropdown.Item onClick={() => isLogout()}>Logout</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
    
      </header>
    </>

  );
};

