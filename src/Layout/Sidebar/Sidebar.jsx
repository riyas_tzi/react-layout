// import './Sidebar.css';
// import { NavLink, } from "react-router-dom";
// import React from 'react';


// const sidebar = (props) => {
//   let isSidebarCollapse = props.closeSidebar;
//   console.log(isSidebarCollapse);
//   let listItems;
//   const NavItems = [
//     { path: "/layout/home", name: "Home" },
//     { path: "/layout/about", name: "About" },
//     { path: "/layout/contact", name: "Contact" },
//   ];

//   listItems = NavItems.map((item, index) => {
//     return (
//       <li className=" p-1 fs-5" key={index} id="sidebarItem">
//         <NavLink
//           to={item?.path}
//           style={{ textDecoration: "none", color: "white" }}
//         >
//           {item?.name}
//         </NavLink>
//       </li>
//     );
//   });

//   return (
//     <div className={`${isSidebarCollapse ? 'sidebar-close' : 'sidebar'}`}>
//       <ul
//         style={{ listStyleType: "none" }}
//         className="d-flex justify-content-start w-auto flex-column ps-0"
//       >
//         {listItems}
//       </ul>
//     </div>
//   );
// };

import './Sidebar.css';
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faBriefcase,
  faPaperPlane,
  faQuestion,
  faImage,
  faCopy,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import Submenu from "./Submenu";
import { Nav, Button } from "react-bootstrap";
import classNames from "classnames";

const Sidebar = (props) => {
  const navListItems = [
    { path: "/layout/home", name: "Home", icon: faHome },
    { path: "/layout/about", name: "About", icon: faBriefcase },
    { path: "/layout/contact", name: "Contact", icon: faPaperPlane },
    { path: "#", name: "Portfolio", icon: faImage },
    { path: "#", name: "FAQ", icon: faQuestion },

  ]

  return (
    <div className={classNames("sidebar", { "is-open": props.isOpen })}>
      <div className="sidebar-header">
        <Button
          variant="link"
          onClick={props.toggle}
          style={{ color: "#fff" }}
          className="mt-4"
        >
          <FontAwesomeIcon icon={faTimes} pull="right" size="xs" />
        </Button>
        <h3>react-bootstrap sidebar</h3>
      </div>

      <Nav className="flex-column pt-2">
        <p className="ms-3" style={{color: "azure"}}>Heading</p>
        {
          navListItems ? navListItems.map((value, index) => {
            return (
              <Nav.Item className="active" key={index}>
                <Nav.Link href={value?.path}>
                  <FontAwesomeIcon icon={value?.icon} className="me-2" />
                  {value?.name}
                </Nav.Link>
              </Nav.Item>
            )
          }) : null
        }

        {/* <Submenu
          title="Pages"
          icon={faCopy}
          items={["Link", "Link2", "Active"]}
        /> */}
      </Nav>
    </div>
  )
}

export default Sidebar; 
