import './Content.css';
import React from "react";
import classNames from "classnames";
import { Container } from "react-bootstrap";
import Header from "./Navbar";
import { Outlet } from "react-router-dom";


const Content = (props) => {
    return (
        <>
            <Container
                fluid
                className={classNames("content", { "is-open": props.isOpen })}
            >
                <Header toggle={props.toggle} />
                <Outlet />
            </Container>
        </>
    )
}

export default Content;
