import "./App.css";
import React, { useState, useEffect,  } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Sidebar from "./Layout/Sidebar/Sidebar";
import Content from "./Layout/Content/Content";

const App = () => {
  const [isOpen, setIsOpen] = useState(false);
  // const Context = createContext();
  // const [name] = useState("hi everyone use me everywhere");
  const [previousWidth, setPreviousWidth] = useState(-1);

  useEffect(() => {
    updateWidth();
    window.addEventListener("resize", updateWidth());
    return () => {
      // console.log("return from use effect");
      window.removeEventListener("resize", updateWidth);
    };
  });

  const updateWidth = () => {
    const width = window.innerWidth;
    const widthLimit = 768;
    const isMobile = width <= widthLimit;
    const wasMobile = previousWidth <= widthLimit;

    if (isMobile !== wasMobile) {
      setIsOpen(!isMobile);
    }
    setPreviousWidth(width);
  };

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="App wrapper">
      <Sidebar toggle={toggle} isOpen={isOpen} />
      <Content toggle={toggle} isOpen={isOpen} />
    </div>
  );
};

export default App;
