import { useState, memo, useCallback, } from "react";

const Callback = () => {
  const [count, setCount] = useState(0);
  const [todos, setTodos] = useState([]);
  // const value=useContext();
  const increment = () => {
    setCount((c) => c + 1);
  };
  const addTodo = useCallback(() => {
    setTodos((t) => [...t, "New Todo"]);
  }, []);
 
  return (
    <>
      <Todos todos={todos} addTodo={addTodo} />
      <hr />
      <div>
        Count: {count}
        <button onClick={increment}>+</button>
        {/* {value} */}
      </div>
    </>
  );
};

const Todos = ({ todos, addTodo }) => {
  console.log("child render");
  return (
    <>
      <h2>My Todos</h2>
      {todos.map((todo, index) => {
        return <p key={index}>{todo}</p>;
      })}
      <button onClick={addTodo}>Add Todo</button>
     
    </>
  );
};
memo(Todos);
export default Callback;
