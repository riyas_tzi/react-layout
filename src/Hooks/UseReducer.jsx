import { useReducer } from "react";

const UseReducer = () => {
   const [count, dispatch] = useReducer(reducer, { count: 0 })
   function reducer(state, action) {
      switch (action.type) {
         case "increment":
            return {
               count: state.count + 1
            }
         case "decrement":
            return {
               count: state.count - 1
            }
         default:
            return { state };
      }

   }
   const handleClickforIncrement = () => {
      dispatch({ type: "increment" })
   }
   const handleClickforDecrement = () => {
      dispatch({ type: "decrement" })
   }

   return (
      <>
         <h1>counter Example Using UseReducer</h1>
         <h4>Count</h4>
         <p>{count.count}</p>
         <div className="d-flex ">
            <button className="btn btn-warning me-3" onClick={handleClickforIncrement} >Increment</button>
            <button className="btn btn-danger" onClick={handleClickforDecrement} >Decrement</button>
         </div>
      </>
   )
}

export default UseReducer;