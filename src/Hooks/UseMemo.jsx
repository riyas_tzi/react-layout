import { useMemo, useState, } from "react";

const Hooks = ({ route }) => {
  const [num, setNum] = useState(0);
  const [todos, setTodos] = useState([]);

  const CalculateFunc = useMemo(() => {
    return expensiveCalculation(num);
  }, [num]);
  const addTodo = () => {
    setTodos((t) => [...t, "New Todo"]);
  };


  return (
    <>
      <div className="container-fluid text-center my-3">
        <h2>Hooks Example</h2>
        <input
          className="form-control w-25 m-auto my-3"
          type={"number"}
          value={num}
          onChange={(e) => setNum(parseInt(e.target.value))}
        />
        {"value " + CalculateFunc}

        <div>
          <br />
          <h2>My Todos</h2>
          {todos.map((todo, index) => {
            return <p key={index}>{todo}</p>;
          })}
          <button onClick={addTodo}>Add Todo</button>
        </div>
      </div>
    </>
  );
};

const expensiveCalculation = (num) => {
  console.log("Calculating...");
  for (let i = 0; i < 1000000000; i++) {
    num += 1;
  }
  return num;
};

export default Hooks;
