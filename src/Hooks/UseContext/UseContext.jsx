
import { createContext, useState, } from "react";
import Component1 from "./Component1";
import Component2 from "./Component2";
export const UserContext = createContext();

const Context = () => {
  const [user] = useState({ name: "Mohamed Riyas", age: 21 });
  const [dark, setTheme] = useState(false);
  const toggle = () => {
    setTheme(data => !data);
  }
  return (
    <UserContext.Provider value={{ name: 'Mohamed Riyas', age: 21, dark: dark }}>
      <div style={{
        // border: "1px solid black",
        padding: "10px",
        background: dark ? "#333" : '#fff',
        color: dark ? '#fff' : '#333',
        transition: "all",
      }}><button className="btn btn-primary" onClick={toggle}>Change Theme</button>
        <h4>{`Hello ${user.name}!`}</h4>
        <h4>{`Your Age  Is ${user.age}`}</h4>
        <hr />
        <Component1 />
        <Component2 />
      </div>

    </UserContext.Provider>
  );
};


export default Context;
