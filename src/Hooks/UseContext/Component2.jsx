

import { UserContext } from "./UseContext";
import { Component } from "react";

export default class Component2 extends Component {

  render() {
    return (

      <UserContext.Consumer >
        {
          (value) => {
            return (
              <>
                <div>
                  <h1>Class Based Component</h1><h4>Hello {value.name} Again</h4><h4>Your Age is  {value.age} Again</h4>

                </div>
              </>
            )
          }
        }

      </UserContext.Consumer>
    )
  }

}
