import { useContext } from "react";
// import { useState } from "react";
import { UserContext } from "./UseContext";


const Component1 = () => {

  const value = useContext(UserContext)
  return (<>
    <div >
      <h1>Function Based Component</h1>
      <h4>Hello {value.name} Again</h4><h4>Your Age is {value.age} Again</h4>
    </div>

  </>
    // <div style={{backgroundColor:dark?'black':'white',color:dark?'white':'black'}}> 

    // </div>
  );
}
export default Component1;