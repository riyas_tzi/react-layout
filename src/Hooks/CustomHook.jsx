import { useState, useEffect } from "react";

const CustomHook = () => {
  const [data] = useFetch("https://jsonplaceholder.typicode.com/todos");

  return (
    <>
      <div className="container-fluid text-center">
       <h1>Custom Hooks Example</h1> 
      {data &&
        data.map((item) => {
          return <p key={item.id}>{item.title}</p>;
        })}
      </div>
    
    </>
  );
};

const useFetch = (url) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => setData(data));
  }, [url]);

  return [data];
};

export default CustomHook;
