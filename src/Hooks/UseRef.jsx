import React, { useState, useEffect, useRef ,useLayoutEffect} from "react";

const Ref = () => {
  const [inputValue, setInputValue] = useState("");
  const previousInputValue = useRef("");
  // const myBtn = React.useRef(null);
  // const handleBtn = () => myBtn.current.click();
  // const [count, setCount] = useState(60);

  // const showCount = React.useCallback(() => {
  //     alert(`Count ${count}`);
  // }, [count])



  useEffect(() => {
    previousInputValue.current = inputValue;
  }, [inputValue]);

  return (
    <>
      <div className="container mx-auto text-center my-5">
        <input
          className="form-control w-50 m-auto mb-4"
          type="text"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        />
        <h2>Current Value: {inputValue}</h2>
        <h2>Previous Value: {previousInputValue.current}</h2>
 
         
        
        {/* <button className="btn btn-dark" ref={myBtn} onChange={handleBtn} >
          Click Me!
        </button> */}
        
      </div>
    </>
  );
}

export default Ref;
