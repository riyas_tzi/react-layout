import './Otppage.css';
import { useLocation, useNavigate } from "react-router-dom";
import { useState } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const Otppage = () => {
    const [first, setFirst] = useState('');
    const [second, setSecond] = useState('');
    const [third, setThird] = useState('');
    const [four, setFour] = useState('');
    const [five, setFive] = useState('');
    const [six, setSix] = useState('');

    const location = useLocation();
    const navigate = useNavigate();
    const id = location.state.user_id
    const onSave = (e) => {
        e.preventDefault();
        const otp = first + second + third + four + five + six;
        if (otp) {
            //  console.log(otp);
            axios.post('http://localhost:3000/api/auth/fp-otp-verify', {
                userId: id,
                otp: otp
            }).then((data) => {


                if (data.data.status === true) {
                    toast.success(data?.data?.message)
                    setTimeout(() => {
                        navigate('/auth/reset-password', {
                            state: {
                                user_id: id
                            }
                        })
                        setFirst('')
                        setSecond('')
                        setThird('')
                        setFour('')
                        setFive('')
                        setSix('')
                    }, 3000)
                }
                if (data?.data?.message === "Expired OTP Time") {
                    toast.error(data?.data?.message)
                }

            }).catch((err) => {

                if (err?.response.data.message === "Invalid OTP") {
                    toast.error(err?.response.data?.message)
                }
            })

        }

    }
    return (
        <>
            <ToastContainer />
            <div className=" d-flex justify-content-center align-items-center vh-100 w-100" id="container">

                <div className="card p-2 text-center " id="card">
                    <form autoComplete='false' onSubmit={onSave}>
                        <h6>Expires Within 5 Minutes</h6>
                        <h6 style={{ color: 'burlywood !important' }}>Please enter the one time password  to verify your account</h6>
                        <div className="text-white"> <span>A code has been sent to</span> <small>*******9897</small> </div>
                        <div id="otp" className="inputs d-flex flex-row justify-content-center mt-2">
                            <input
                                className="m-2 text-center form-control rounded"
                                id="first" maxLength="1" required
                                value={first} onChange={(e) => setFirst(e.target.value)}
                            />
                            <input
                                className="m-2 text-center form-control rounded"
                                id="second" maxLength="1" required
                                value={second} onChange={(e) => setSecond(e.target.value)}
                            />
                            <input
                                className="m-2 text-center form-control rounded"
                                id="third" maxLength="1" required
                                value={third} onChange={(e) => setThird(e.target.value)}
                            />
                            <input
                                className="m-2 text-center form-control rounded"
                                id="fourth" maxLength="1" required
                                value={four} onChange={(e) => setFour(e.target.value)}
                            />
                            <input
                                className="m-2 text-center form-control rounded"
                                id="fifth" maxLength="1" required
                                value={five} onChange={(e) => setFive(e.target.value)}
                            />
                            <input
                                className="m-2 text-center form-control rounded"
                                id="sixth" maxLength="1" required
                                value={six} onChange={(e) => setSix(e.target.value)}

                            />
                        </div>

                        <div className="mt-4"> <button type="submit" className="btn btn-danger px-4 validate">Validate</button> </div>
                    </form>
                </div >

            </div >
        </>
    )
}

export default Otppage;