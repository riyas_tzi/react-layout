// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ForgetPassword = () => {
    const [email, setEmail] = useState('')
    const navigate = useNavigate()
    const onSave = (e) => {
        if (email) {
            e.preventDefault();
            
            axios.post("http://localhost:3000/api/auth/fp-mail-verify", { email: email }, {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
            }).then((res) => {
                
                if (res?.data?.status === true) {
                    toast.success(res?.data?.message);
                    let id = res?.data?.userDetail?.id;
                    setTimeout(()=>{
                        navigate('/auth/otppage', {
                            state: {
                                user_id: id
                            }
                        })
                        setEmail('');
                    },3000)
                    
                }
                else { toast.error(res?.data?.message) }

            }).catch((data) => {
               
                if (data?.response?.data?.status === false)
                    toast.error(data?.response?.data?.message)
            })
        }
    }
    return (
        <>  <ToastContainer />
            <div className="container my-5">
                <div className="row justify-content-center">
                    <div className="col-md-4 col-md-offset-4">
                        <div className="card">
                            <div className="card-body">
                                <div className="text-center">
                                    <h3><i className="fa fa-lock fa-4x"></i></h3>
                                    <h2 className="text-center">Forgot Password?</h2>
                                    <p>You can reset your password here.</p>


                                    <form id="register-form" autoComplete="off" className="form" method="post" onSubmit={onSave}>

                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-addon border p-2 bg-secondary bg-gradient"><i className="fa fa-light fa-envelope mt-2"></i></span>
                                                <input
                                                    placeholder="email address"
                                                    className="form-control"
                                                    type="email"
                                                    value={email}
                                                    onChange={(e) => setEmail(e.target.value)}
                                                    required
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group mt-3">
                                            <input className="btn btn-primary " value="Reset Password" type="submit" />
                                        </div>

                                        <input type="hidden" className="hide" name="token" id="token" value="" />
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ForgetPassword;