import React, { Component } from "react";
import axios from 'axios'
import ReactSelect from "react-select";

class Signup extends Component {

  constructor() {
    super();
    this.state = {
      form: {
        name: "",
        email: "",
        mobile: "",
        password: "",
        confirmPassword: "",
        gender: null,
        country: null || "",
      },
      formErrors: {
        name: null,
        email: null,
        mobile: null,
        password: null,
        confirmPassword: null,
        gender: null,
        country: null,
      },
    };
    this.countryList = [
      { value: "india", label: "India" },
      { value: "us", label: "US" },
      { value: "australia", label: "Australia" },
    ];
  }

  componentDidMount() {
    console.log('component mount');
    // axios.get('http://localhost:3000/api/signup/getall').then((res) => {
    //   console.log(res?.data?.data)
    // })
  }

  validateNumber = (evt) => {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === "paste") {
      var key = theEvent.clipboardData.getData("text/plain");
    } else {
      // Handle key press
      key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
      theEvent.returnValue = false;
      if (theEvent.preventDefault) theEvent.preventDefault();
    }
  };

  handleChange = (e) => {
    const { name, value, checked } = e.target;
    const { form, formErrors } = this.state;
    let formObj = {};
    if (name === "language") {
      // handle the change event of language field
      if (checked) {
        // push selected value in list
        formObj = { ...form };
        formObj[name].push(value);
      } else {
        // remove unchecked value from the list
        formObj = {
          ...form,
          [name]: form[name].filter((x) => x !== value),
        };
      }
    } else {
      // handle change event except language field
      formObj = {
        ...form,
        [name]: value,
      };
    }
    this.setState({ form: formObj }, () => {
      if (!Object.keys(formErrors).includes(name)) return;
      let formErrorsObj = {};
      if (name === "password" || name === "confirmPassword") {
        let refValue =
          this.state.form[name === "password" ? "confirmPassword" : "password"];
        const errorMsg = this.validateField(name, value, refValue);
        formErrorsObj = { ...formErrors, [name]: errorMsg };
        if (!errorMsg && refValue) {
          formErrorsObj.confirmPassword = null;
          formErrorsObj.password = null;
        }
      } else {
        const errorMsg = this.validateField(name, value);
        formErrorsObj = { ...formErrors, [name]: errorMsg };
      }
      this.setState({ formErrors: formErrorsObj });
    });
  };

  validateField = (name, value, refValue) => {
    let errorMsg = null;
    switch (name) {
      case "name":
        if (!value) errorMsg = "Please enter Name.";
        break;
      case "email":
        if (!value) errorMsg = "Please enter Email.";
        else if (
          !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            value
          )
        )
          errorMsg = "Please enter valid Email.";
        break;
      case "mobile":
        if (!value) errorMsg = "Please enter Mobile.";
        break;
      case "country":
        if (!value) errorMsg = "Please select Country.";
        break;
      case "gender":
        if (!value) errorMsg = "Please select Gender.";
        break;
      case "password":
        // refValue is the value of Confirm Password field
        if (!value) errorMsg = "Please enter Password.";
        else if (refValue && value !== refValue)
          errorMsg = "Password and Confirm Password does not match.";
        break;
      case "confirmPassword":
        // refValue is the value of Password field
        if (!value) errorMsg = "Please enter Confirm Password.";
        else if (refValue && value !== refValue)
          errorMsg = "Password and Confirm Password does not match.";
        break;
      case "language":
        if (value.length === 0) errorMsg = "Please select Language.";
        break;
      default:
        break;
    }
    return errorMsg;
  };

  validateForm = (form, formErrors, validateFunc) => {
    const errorObj = {};
    Object.keys(formErrors).map((x) => {
      //   return()
      let refValue = null;
      if (x === "password" || x === "confirmPassword") {
        refValue = form[x === "password" ? "confirmPassword" : "password"];
      }
      const msg = validateFunc(x, form[x], refValue);
      if (msg) errorObj[x] = msg;
      return errorObj;
    });
    return errorObj;
  };

  handleSubmit = () => {
    debugger;
    const { form, formErrors } = this.state;
    const errorObj = this.validateForm(form, formErrors, this.validateField);
    if (Object.keys(errorObj).length !== 0) {
      this.setState({ formErrors: { ...formErrors, ...errorObj } });
      return false;
    } else {

      axios.post('http://localhost:3000/api/auth/signup/', form,).then((data) => {
        // console.log(data);
        if (data.data.status === true)
          alert(data.data.message)
      }).catch((data) => {
        // console.log(data);

      })
      this.reset();
    }
  };

  reset() {
    this.setState(
      (cur) => {
        return {
          ...cur,
          form: {
            ...cur.form,
            name: "",
            email: "",
            mobile: "",
            password: "",
            confirmPassword: "",
            gender: null,
            country: null || "",
          },
        };
      })
  }
  render() {
    const { form, formErrors } = this.state;
    return (
      <>
        <div className="container-fluid">
          <div className="signup-box w-50 ">
            <p className="title fs-4 d-flex justify-content-center">Sign up</p>
            <div className="row">
              <div className="col-md-12">
                <div className="form-group mb-2">
                  <label>
                    Name:<span className="asterisk">*</span>
                  </label>
                  <input
                    className={`form-control mt-2  ${formErrors.name !== null ? "border-danger" : ""
                      } `}
                    type="text"
                    name="name"
                    value={form.name}
                    onChange={this.handleChange}
                    onBlur={this.handleChange}
                  />
                  {formErrors.name && (
                    <span className="err">{formErrors.name}</span>
                  )}
                </div>
                <div className="form-group mb-2">
                  <label>
                    Email:<span className="asterisk">*</span>
                  </label>
                  <input
                    className={`form-control mt-2  ${formErrors.email !== null ? "border-danger" : ""
                      } `}
                    type="text"
                    name="email"
                    value={form.email}
                    onChange={this.handleChange}
                    onBlur={this.handleChange}
                  />
                  {formErrors.email && (
                    <span className="err">{formErrors.email}</span>
                  )}
                </div>
                <div className="form-group mb-2">
                  <label>
                    Password:<span className="asterisk">*</span>
                  </label>
                  <input
                    className={`form-control mt-2  ${formErrors.password !== null ? "border-danger" : ""
                      } `}
                    type="password"
                    name="password"
                    value={form.password}
                    onChange={this.handleChange}
                    onBlur={this.handleChange}
                  />
                  {formErrors.password && (
                    <span className="err">{formErrors.password}</span>
                  )}
                </div>
                <div className="form-group mb-2">
                  <label>
                    Confirm Password:<span className="asterisk">*</span>
                  </label>
                  <input
                    className={`form-control mt-2  ${formErrors.confirmPassword !== null ? "border-danger" : ""
                      } `}
                    type="password"
                    name="confirmPassword"
                    value={form.confirmPassword}
                    onChange={this.handleChange}
                    onBlur={this.handleChange}
                  />
                  {formErrors.confirmPassword && (
                    <span className="err">{formErrors.confirmPassword}</span>
                  )}
                </div>
                {/* </div>
             <div className="col-md-6"> */}
                <div className="form-group mb-2">
                  <label>
                    Mobile:<span className="asterisk">*</span>
                  </label>
                  <input
                    className={`form-control mt-2  ${formErrors.mobile !== null ? "border-danger" : ""
                      } `}
                    type="text"
                    name="mobile"
                    value={form.mobile}
                    onChange={this.handleChange}
                    onBlur={this.handleChange}
                    onKeyPress={this.validateNumber}
                  />
                  {formErrors.mobile && (
                    <span className="err">{formErrors.mobile}</span>
                  )}
                </div>
                <div className="form-group mb-2">
                  <label className="me-3">
                    Gender:<span className="asterisk">*</span>
                  </label>
                  <div className="form-control border-0 p-0 pt-1 mt-2">
                    <label className="me-3">
                      <input
                        type="radio"
                        name="gender"
                        value="male"
                        checked={form.gender === "male"}
                        onChange={this.handleChange}
                      />
                      Male
                    </label>
                    <label>
                      <input
                        type="radio"
                        name="gender"
                        value="female"
                        checked={form.gender === "female"}
                        onChange={this.handleChange}
                      />{" "}
                      Female
                    </label>
                  </div>
                  {formErrors.gender && (
                    <span className="err">{formErrors.gender}</span>
                  )}
                </div>

                <div className="form-group mb-2">
                  <label>
                    Country:<span className="asterisk">*</span>
                  </label>
                  <ReactSelect
                    className={`form-control mt-2  ${formErrors.country !== null ? "border-danger" : ""
                      } `}
                    name="country"
                    options={this.countryList}
                    value={this.countryList.find(
                      (x) => x.value === form.country
                    )}
                    onChange={(e) =>
                      this.handleChange({
                        target: {
                          name: "country",
                          value: e.value,
                        },
                      })
                    }

                  />

                  {/* <select className={`form-control mt-2  ${formErrors.country !== null ? "border-danger" : ""
                    } `}
                    data-mdb-filter="true" >
                    {
                      this.countryList.map((val, idx) => {
                        return (
                          <option key={val}
                            value={this.countryList.find(
                              (x) => x.value === form.country
                            )}
                            // value={form.country}
                            onChange={(e) =>
                              this.handleChange({
                                target: {
                                  name: "country",
                                  value: e.value,
                                },
                              })} > {val.label}</option>
                        )
                      })
                    }
                  </select> */}
                  {formErrors.country && (
                    <span className="err">{formErrors.country}</span>
                  )}
                </div>
              </div>
            </div>

            <div className="form-group mt-3 d-flex justify-content-center">
              <input
                type="button"
                className="btn btn-primary"
                value="Submit"
                onClick={this.handleSubmit}
              />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Signup;
