import './Login.css';
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Auth from "../Auth";



async function LoginUser(credentials) {
  return axios
    .post("http://localhost:3000/api/auth/signin", credentials, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
    .then((data) => {
      // console.log(data)
      toast.success(data.data.message)
      return data.data.Token;

    }).catch((data) => {
      console.log(data)
      if (data.response.data.message === "Invalid Email Address") {
        toast.error(data.response.data.message)
      }
      if (data.response.data.message === "Invalid Password") {
        toast.error(data.response.data.message)
      }
    })


}

const Login = () => {

  const [validated, setValidated] = useState(false);
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const { isLogin } = Auth();
  const navigate = useNavigate();
  const handleSubmit = async (event) => {

    event.preventDefault();
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      const token = await LoginUser({
        email,
        password,
      });

      if (token) {
        isLogin(token);
        navigate("/layout/home");
      }

    }
    setValidated(true);
  };

  return (
    <div className="loginContainer">
      <ToastContainer />

      <Card
        className="w-50 m-auto position-absolute top-50 start-50 translate-middle card ">
        <Card.Title className="d-flex justify-content-center mt-3">
          Login
        </Card.Title>
        <Card.Body>

          <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Form.Group controlId="validationCustom01" className="mb-3">
              <Card.Text className="mb-0">
                <Form.Label> Enter Email</Form.Label>
              </Card.Text>
              <Form.Control
                required
                type="email"
                placeholder="Email Address"
                onChange={(e) => setEmail(e.target.value)}
              />
              <Form.Control.Feedback  >Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                Email is required.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="validationCustom02" className="mb-3">
              <Card.Text className="mb-0">
                <Form.Label>Enter Password</Form.Label>
              </Card.Text>
              <Form.Control
                required
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                Password is required.
              </Form.Control.Feedback>
            </Form.Group>
            <div className="mt-3 d-flex justify-content-center">
              <Button type="submit">Submit form</Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
};

export default Login;
