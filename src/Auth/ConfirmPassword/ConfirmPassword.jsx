import './ConfirmPassword.css'
import { useState } from 'react'
import { useLocation } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';



const ConfirmPassword = () => {
    const [eyetoggle, setEyetoggle] = useState(false);
    const location = useLocation();
    let id = location.state.user_id;
    // console.log(id);
    const [conPassword, setConPassword] = useState('');

    const onSave = (e) => {
        e.preventDefault();
        if (conPassword) {
            axios.post('http://localhost:3000/api/auth/fp-reset-password',
                {
                    userId: id,
                    reset_password: conPassword
                }).then((data) => {
                    // console.log('success', data);
                    if (data?.data?.message === "Password Resetting Failed") {
                        toast.error(data?.data?.message)
                    }
                    if (data?.data?.status === true) {
                        toast.success(data?.data?.message)
                        setConPassword('')
                    }

                }).catch((err) => {
                    // console.log('error', err);
                    if (err?.response?.data?.status === false)
                        toast.error(err?.response?.data?.message)
                })
        }
    }

    return (
        <>
            <ToastContainer />
            <div
                className="w-100 vh-100 d-flex justify-content-center align-items-center "
                id="container"
            >
                <div className="card shadow border-0 rounded p-4  d-flex justify-content-center bg-secondary w-25" id="card">
                    <form onSubmit={onSave}>
                        <h3>Your Confirm Password</h3>
                        <div className="formgroup">
                            <div className="input-group input-group-alternative  mt-4 mb-3">
                                <input
                                    type={eyetoggle ? 'text' : 'password'}
                                    className="form-control pl-2"
                                    value={conPassword}
                                    onChange={(e) => setConPassword(e.target.value)}
                                    placeholder="Enter Confirm Password"
                                    required
                                />
                                <button
                                    className="input-group-addon" id="eye-icon"
                                    type="button"
                                    onClick={() => setEyetoggle(!eyetoggle)}
                                >
                                    {
                                        eyetoggle ? <i className='fa fa-eye' /> : <i className='fa fa-eye-slash' />
                                    }


                                </button>
                            </div>
                        </div>
                        <button type="submit" id="login-btn" className="my-4">Next</button>
                    </form>
                </div>
            </div>

        </>
    )
}

export default ConfirmPassword;