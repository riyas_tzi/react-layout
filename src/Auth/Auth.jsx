import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Auth = () => {
  const navigate = useNavigate();
  const getToken = () => {

    const tokenString = localStorage.getItem("authToken");
    const userToken = JSON.parse(tokenString);
    return userToken;
  };

  const [token, setToken] = useState(getToken());
  const saveToken = (userToken) => {
    debugger
    localStorage.setItem("authToken", JSON.stringify(userToken));
    setToken(userToken);
  };
  const removeToken = () => {
    localStorage.removeItem('authToken');
    navigate('/')

  }
  return {
    isLogin: saveToken,
    isLogout: removeToken,
    token,
  };
};

export default Auth;
