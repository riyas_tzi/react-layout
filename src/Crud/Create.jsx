
import axios from 'axios';
import React, { useState, useEffect, } from 'react';
import { useNavigate, useParams } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';
import ReactSelect from "react-select";
import Card from 'react-bootstrap/Card';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Create = () => {
    const [validated, setValidated] = useState(false);
    const [selectValidation, setSelectValidation] = useState(false)
    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [gender, setGender] = useState('');
    const [country, setCountry] = useState(1);
    const [file, setFile] = React.useState('');
    const baseUrl = 'http://localhost:3000';
    let navigate = useNavigate();
    let { id } = useParams();
    // console.log(id);
    const getData = () => {
        axios.post(`${baseUrl}/api/crud/get`, { id: id }).then((res) => {
            if (res) {
                console.log(res);
                setFname(res?.data?.data?.fname.trim());
                setLname(res?.data?.data?.lname.trim());
                setEmail(res?.data?.data?.email.trim());
                setMobile(res?.data?.data?.mobile.trim());
                setGender(res?.data?.data?.gender.trim());
                setCountry(res?.data?.data?.country.trim());

            }

        }).catch((err) => {
            console.log(err);
        })
    }

    useEffect(() => {
        getData();
    }, [id]);

    const countryList = [
        { value: "India", label: "India" },
        { value: "usa", label: "USA" },
        { value: "australia", label: "Australia" },
    ];

    const handleClick = () => {
        navigate('/layout/crud-list');
    }
    const handleChangeForNum = (event) => {
        const newValue = event.target.value.replace(/\D/g, "").slice(0, 10);
        setMobile(newValue);
    };

    const handleChange = (e) => {
        const { name, value, files } = e.target;
        if (name === "gender") {
            setGender(value);
            // console.log(value);
        }
        if (name === "file") {
            if (files && files[0]) {
                setFile(files[0]);
            }
        }
    }

    const handleChangeForCountry = (e) => {
        setCountry(e.value);
        setSelectValidation(false)

    }

    const handleSubmit = (event) => {
        event.preventDefault();
        if (id !== undefined) {
            // update
            const formdata = new FormData();
            formdata.append('fname', fname);
            formdata.append('lname', lname);
            formdata.append('email', email);
            formdata.append('mobile', mobile);
            formdata.append('gender', gender);
            formdata.append('country', country);
            formdata.append('file', file);
            formdata.append('id', id);

            axios.post(`${baseUrl}/api/crud/update`, formdata, {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
            }).then((data) => {
                console.log(data);
                if (data.data.status === true) {
                    toast.success(data.data.message);
                    setTimeout(() => {
                        navigate('/layout/crud-list');
                    }, 3000);
                }
                if (data.data.status === false) {
                    toast.error(data.data.message);
                }

            }).catch((data) => {
                console.log(data);
                toast.error(data.data.message);
            })
        } else {
            // create 
            const form = event.currentTarget;
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                setSelectValidation(true);
                toast.error('please fill all required fields')
            } else {
                setSelectValidation(false);
                const formdata = new FormData();

                formdata.append('fname', fname);
                formdata.append('lname', lname);
                formdata.append('email', email);
                formdata.append('mobile', mobile);
                formdata.append('gender', gender);
                formdata.append('country', country);
                formdata.append('file', file);


                axios.post(`${baseUrl}/api/crud/create`, formdata, {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                }).then((data) => {
                    console.log(data);
                    if (data.data.status === true) {
                        toast.success(data.data.message);
                        setTimeout(() => {
                            navigate('/layout/crud-list');
                        }, 3000);

                    }
                    if (data.data.status === false) {
                        toast.error(data.data.message);
                    }

                }).catch((data) => {
                    console.log(data);
                    toast.error(data.data.message);
                })
            }
            setValidated(true);
        }
    };

    return (
        <div className='container-fluid'>
            <ToastContainer />
            <h1 className='text-center'>Crud Operations</h1>
            <div className='d-flex justify-content-end pe-5 mb-2'>
                <Button variant='primary' onClick={handleClick} >View Records</Button>
            </div>
            <Card className="h-auto mx-auto w-75 p-2">
                <Card.Body>
                    <Form noValidate validated={validated} onSubmit={handleSubmit} >
                        <Row className="mb-3">
                            <Form.Group as={Col} md="6" >
                                <Form.Label>First name</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Control
                                        required
                                        type="text"
                                        placeholder="First name"
                                        value={fname}
                                        name="fname"
                                        onChange={(e) => setFname(e.target.value)}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        Please Choose a First Name.
                                    </Form.Control.Feedback>

                                </InputGroup>

                            </Form.Group>
                            <Form.Group as={Col} md="6" >
                                <Form.Label>Last name</Form.Label>
                                <InputGroup hasValidation>
                                    <Form.Control
                                        required
                                        type="text"
                                        placeholder="Last name"
                                        value={lname}
                                        name="lname"
                                        onChange={(e) => setLname(e.target.value)}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        Please Choose a Last Name.
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>

                        </Row>
                        <Row className="mb-3">
                            <Form.Group as={Col} md="6" >
                                <Form.Label>Email</Form.Label>
                                <InputGroup hasValidation>
                                    <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                                    <Form.Control
                                        type="email"
                                        placeholder="Email"
                                        aria-describedby="inputGroupPrepend"
                                        required
                                        name="email"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        Please choose a email.
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group as={Col} md="6" >
                                <Form.Label>Mobile No</Form.Label>

                                <Form.Control
                                    type="text"
                                    placeholder="Mobile Number"
                                    required
                                    value={mobile}
                                    name="mobile"
                                    onChange={handleChangeForNum}
                                    maxLength={10}
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please choose a mobile.
                                </Form.Control.Feedback>

                            </Form.Group>

                        </Row>
                        <Row className="mb-3">
                            {['radio'].map((type) => (
                                <Form.Group as={Col} md="6"
                                    key={`inline-${type}`}>
                                    <Form.Label>Gender</Form.Label><br />
                                    <Form.Check
                                        inline
                                        label="Male"
                                        name="gender"
                                        type={type}
                                        id={`inline-${type}-1`}
                                        required
                                        value="male"
                                        checked={gender === 'male'}
                                        onChange={handleChange}
                                    />
                                    <Form.Check
                                        inline
                                        label="Female"
                                        name="gender"
                                        type={type}
                                        id={`inline-${type}-2`}
                                        required
                                        value='female'
                                        checked={gender === 'female'}
                                        onChange={handleChange}
                                    />
                                    <Form.Check
                                        inline
                                        name="gender"
                                        label="Others"
                                        type={type}
                                        id={`inline-${type}-3`}
                                        required
                                        value='others'
                                        checked={gender === 'others'}
                                        onChange={handleChange}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        Please choose a gender.
                                    </Form.Control.Feedback>
                                </Form.Group>

                            ))}

                            <Form.Group as={Col} md="6">
                                <Form.Label>Country</Form.Label>
                                <ReactSelect
                                    className={`border  ${selectValidation ? 'border-danger' : ''} `}
                                    required
                                    name="country"
                                    options={countryList}
                                    value={countryList.find(
                                        (x) => x.value === country
                                    )}
                                    onChange={handleChangeForCountry}

                                />
                            </Form.Group>

                        </Row>
                        <Row className="mb-3">
                            <Form.Group>
                                <Form.Label>File</Form.Label>
                                <Form.Control
                                    type="file"
                                    required
                                    name="file"
                                    // value={file || ''}
                                    defaultValue={file}
                                    onChange={handleChange}
                                    //   isInvalid={!!errors.file}
                                    accept={'*'}
                                />
                            </Form.Group>

                        </Row>
                        <div className='d-flex justify-content-center'>
                            <Button type="submit" className='px-5'>{id ? 'Update' : 'Create'}</Button>
                        </div>
                    </Form>
                </Card.Body>

            </Card>
            {/* <Card>
                <Card.Body>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Recusandae quae iure laudantium dolorem obcaecati iusto adipisci tempore. Tempora, provident beatae?
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem necessitatibus possimus molestias ipsam minima itaque sit amet est doloribus temporibus atque praesentium reprehenderit quo commodi iste aliquid, esse culpa repudiandae!
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam cum vitae quisquam, beatae dicta deserunt. Maiores laudantium sed excepturi odio dolor voluptatem, quaerat, et dolorum fugit nostrum ipsam distinctio numquam.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, quod architecto fuga rem magni officia perspiciatis. Voluptatem itaque, nemo error enim pariatur obcaecati perspiciatis iure alias ducimus! Voluptates, unde facilis.
                    
                </Card.Body>
            </Card> */}
        </div>
    );
}

export default Create;