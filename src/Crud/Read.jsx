
import axios from "axios";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import Table from 'react-bootstrap/Table'
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer, } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const View = () => {
    const baseUrl = 'http://localhost:3000';
    const navigate = useNavigate();
    const [records, setRecords] = useState([]);

    useEffect(() => {
        getAllData();
    }, [])

    const getAllData = () => {
        axios.get(`${baseUrl}/api/crud/getall`).then((res) => {
            // console.log(res);
            if (res.data.status === true) {
                setRecords(res.data.data);
            }
        })
    }
    const edit = (e, id) => {
        e.preventDefault();
        // console.log(id);
        navigate(`/layout/crud-create/${id}`);
    }
    const del = (e, id) => {
        e.preventDefault();
        axios.post(`${baseUrl}/api/crud/delete`, { id: id }).then((res) => {
            console.log(res);
            if (res.data.status === true) {
                toast.success(res.data.message);
                getAllData();
            }
        })
    }
    return (
        <div className="row">
            <ToastContainer />
            <Table striped bordered hover style={{ border: 'darkgrey' }}>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Gender</th>
                        <th>Country</th>
                        <th>Image</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {records ? records.map((val, i) => {
                        return (
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{val?.fname}</td>
                                <td>{val?.lname}</td>
                                <td>{val?.email}</td>
                                <td>{val?.mobile}</td>
                                <td>{val?.gender}</td>
                                <td>{val?.country}</td>
                                <td><img alt="" src={"http://localhost:3000/" + val?.file} width={"30px"} /></td>
                                <td><Button variant="primary" onClick={(e) => edit(e, val?.id)} >Edit</Button></td>
                                <td><Button variant="danger" onClick={(e) => del(e, val?.id)} >Delete</Button></td>
                            </tr>
                        )
                    }) : null
                    }
                </tbody>
            </Table>
        </div>
    )
}

export default View;