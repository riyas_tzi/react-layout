import "./index.css";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Hooks from "./Hooks/UseMemo";
import Context from "./Hooks/UseContext/UseContext";
import Ref from "./Hooks/UseRef";
import Callback from "./Hooks/UseCallback";
import CustomHook from "./Hooks/CustomHook";
import Login from "./Auth/Login/Login";
import Signup from "./Auth/Signup/Signup";
import ForgetPassword from "./Auth/ForgetPassword";
import OtpPage from "./Auth/otppage/Otppage";
import ResetPassword from "./Auth/ConfirmPassword/ConfirmPassword";
import Create from "./Crud/Create";
import View from "./Crud/Read";
import Home from "./Components/Home";
import About from "./Components/About";
import Contact from "./Components/Contacts";
import App from "./App";
import UseReducer from "./Hooks/UseReducer";


const root = ReactDOM.createRoot(document.getElementById("root"));
const router = createBrowserRouter([
  {
    path: "/",
    element: <Login />,
  },
  {
    path: "/auth/signup",
    element: <Signup />,
  },
  {
    path: "/auth/forget-password",
    element: <ForgetPassword />,
  },
  {
    path: "/auth/otppage",
    element: <OtpPage />,
  },
  {
    path: "/auth/reset-password",
    element: <ResetPassword />,
  },

  {
    path: "/layout",
    element: <App />,
    children: [
      {
        path: "/layout/home",
        element: <Home />,
      },
      {
        path: "/layout/contact",
        element: <Contact />,
      },
      {
        path: "/layout/about",
        element: <About />,
      },
      {
        path: "/layout/crud-create",
        element: <Create />,
      },
      {
        path: "/layout/crud-create/:id",
        element: <Create />,
      },
      {
        path: "/layout/crud-list",
        element: <View />,
      },
      {
        path: "/layout/hooks/memo",
        element: <Hooks />,
      },
      {
        path: "/layout/hooks/context",
        element: <Context />,
      },
      {
        path: "/layout/hooks/ref",
        element: <Ref />,
      },
      {
        path: "/layout/hooks/callback",
        element: <Callback />,
      },
      {
        path: "/layout/hooks/custom-hook",
        element: <CustomHook />,
      },
      {
        path: "/layout/hooks/use-reducer",
        element: <UseReducer/>,
      },
    ],
  },
]);

root.render(<RouterProvider router={router}></RouterProvider>);
